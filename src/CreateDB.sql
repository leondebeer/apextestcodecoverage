# SQL Script which creates the tables for
# Storing any ApexTestCode related list

Use ApexTestCode;
drop table if exists SalesforceInstance;
drop table if exists User;
drop table if exists ApexClass;
drop table if exists ApexStatistics;
drop table if exists ApexLogRun;

Create table 			SalesforceInstance (
	ID					SERIAL,
	LastUpdate			TIMESTAMP,
	Login				VARCHAR(100),
	OrgID				VARCHAR(20)
);

Create Table			User (
	ID					SERIAL,
	LastUpdate			TIMESTAMP,	
	OrgKey				BIGINT UNSIGNED,
	UserName			VARCHAR(200),
	FirstName			VARCHAR(200),
	LastName			VARCHAR(200),
	Email				VARCHAR(255)
);

Create Table			ApexClass (
	ID					SERIAL,
	LastUpdate			TIMESTAMP,	
	RecordCreated		DATETIME,
	OrgKey				BIGINT 	UNSIGNED,
	CreatedUserKey		BIGINT 	UNSIGNED,
	LastUpdatedUserKey	BIGINT	UNSIGNED,
	ClassName			VARCHAR(200)
);

Create 	Table			ApexLogRun (
	ID					SERIAL,
	LastUpdate			TIMESTAMP,
	OrgKey				BIGINT UNSIGNED,
	RecordCreated		DATETIME
);

Create 	Table			ApexStatistics (
	ID					SERIAL,
	LastUpdate			TIMESTAMP,	
	ApexLogRunID		BIGINT  UNSIGNED,
	ApexClassKey		BIGINT	UNSIGNED,
	LineCount			INT,
	LineCoveredCount	INT,
	LineNotCoveredCount	INT,
	Size				INT,
	ShowAllData			INT,
	AssertCount			INT,
	TestClass			INT	
);